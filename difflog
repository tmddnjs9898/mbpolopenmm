diff --git a/openmmapi/include/openmm/CustomBondForce.h b/openmmapi/include/openmm/CustomBondForce.h
index 76cb8c6..8a5a4df 100644
--- a/openmmapi/include/openmm/CustomBondForce.h
+++ b/openmmapi/include/openmm/CustomBondForce.h
@@ -33,6 +33,7 @@
  * -------------------------------------------------------------------------- */
 
 #include "Force.h"
+#include "TabulatedFunction.h"
 #include "Vec3.h"
 #include <vector>
 #include "internal/windowsExport.h"
@@ -83,6 +84,7 @@ public:
      *                  of r, the distance between them
      */
     explicit CustomBondForce(const std::string& energy);
+    ~CustomBondForce();
     /**
      * Get the number of bonds for which force field parameters have been defined.
      */
@@ -108,6 +110,12 @@ public:
     int getNumEnergyParameterDerivatives() const {
         return energyParameterDerivatives.size();
     }
+    /**
+     * Get the number of tabulated functions that have been defined.
+     */
+    int getNumTabulatedFunctions() const {
+        return functions.size();
+    }
     /**
      * Get the algebraic expression that gives the interaction energy for each bond
      */
@@ -190,6 +198,37 @@ public:
      * @return the parameter name
      */
     const std::string& getEnergyParameterDerivativeName(int index) const;
+    /**
+     * Add a tabulated function that may appear in the energy expression.
+     *
+     * @param name           the name of the function as it appears in expressions
+     * @param function       a TabulatedFunction object defining the function.  The TabulatedFunction
+     *                       should have been created on the heap with the "new" operator.  The
+     *                       Force takes over ownership of it, and deletes it when the Force itself is deleted.
+     * @return the index of the function that was added
+     */
+    int addTabulatedFunction(const std::string& name, TabulatedFunction* function);
+    /**
+     * Get a const reference to a tabulated function that may appear in the energy expression.
+     *
+     * @param index     the index of the function to get
+     * @return the TabulatedFunction object defining the function
+     */
+    const TabulatedFunction& getTabulatedFunction(int index) const;
+    /**
+     * Get a reference to a tabulated function that may appear in the energy expression.
+     *
+     * @param index     the index of the function to get
+     * @return the TabulatedFunction object defining the function
+     */
+    TabulatedFunction& getTabulatedFunction(int index);
+    /**
+     * Get the name of a tabulated function that may appear in the energy expression.
+     *
+     * @param index     the index of the function to get
+     * @return the name of the function as it appears in expressions
+     */
+    const std::string& getTabulatedFunctionName(int index) const;
     /**
      * Add a bond term to the force field.
      *
@@ -246,10 +285,12 @@ private:
     class BondInfo;
     class BondParameterInfo;
     class GlobalParameterInfo;
+    class FunctionInfo;
     std::string energyExpression;
     std::vector<BondParameterInfo> parameters;
     std::vector<GlobalParameterInfo> globalParameters;
     std::vector<BondInfo> bonds;
+    std::vector<FunctionInfo> functions;
     std::vector<int> energyParameterDerivatives;
     bool usePeriodic;
 };
@@ -296,6 +337,20 @@ public:
     }
 };
 
+/**
+ * This is an internal class used to record information about a tabulated function.
+ * @private
+ */
+class CustomBondForce::FunctionInfo {
+public:
+    std::string name;
+    TabulatedFunction* function;
+    FunctionInfo() {
+    }
+    FunctionInfo(const std::string& name, TabulatedFunction* function) : name(name), function(function) {
+    }
+};
+
 } // namespace OpenMM
 
 #endif /*OPENMM_CUSTOMBONDEDFORCE_H_*/
diff --git a/openmmapi/src/CustomBondForce.cpp b/openmmapi/src/CustomBondForce.cpp
index 2b85098..e914b6b 100644
--- a/openmmapi/src/CustomBondForce.cpp
+++ b/openmmapi/src/CustomBondForce.cpp
@@ -47,6 +47,11 @@ using std::vector;
 CustomBondForce::CustomBondForce(const string& energy) : energyExpression(energy), usePeriodic(false) {
 }
 
+CustomBondForce::~CustomBondForce() {
+    for (auto function : functions)
+        delete function.function;
+}
+
 const string& CustomBondForce::getEnergyFunction() const {
     return energyExpression;
 }
@@ -109,6 +114,26 @@ const string& CustomBondForce::getEnergyParameterDerivativeName(int index) const
     return globalParameters[energyParameterDerivatives[index]].name;
 }
 
+int CustomBondForce::addTabulatedFunction(const std::string& name, TabulatedFunction* function) {
+    functions.push_back(FunctionInfo(name, function));
+    return functions.size()-1;
+}
+
+const TabulatedFunction& CustomBondForce::getTabulatedFunction(int index) const {
+    ASSERT_VALID_INDEX(index, functions);
+    return *functions[index].function;
+}
+
+TabulatedFunction& CustomBondForce::getTabulatedFunction(int index) {
+    ASSERT_VALID_INDEX(index, functions);
+    return *functions[index].function;
+}
+
+const string& CustomBondForce::getTabulatedFunctionName(int index) const {
+    ASSERT_VALID_INDEX(index, functions);
+    return functions[index].name;
+}
+
 int CustomBondForce::addBond(int particle1, int particle2, const vector<double>& parameters) {
     bonds.push_back(BondInfo(particle1, particle2, parameters));
     return bonds.size()-1;
diff --git a/platforms/cuda/include/CudaKernels.h b/platforms/cuda/include/CudaKernels.h
index 1a6014c..80b75b3 100644
--- a/platforms/cuda/include/CudaKernels.h
+++ b/platforms/cuda/include/CudaKernels.h
@@ -341,6 +341,7 @@ private:
     const System& system;
     CudaParameterSet* params;
     CudaArray globals;
+    std::vector<CudaArray> tabulatedFunctions;
     std::vector<std::string> globalParamNames;
     std::vector<float> globalParamValues;
 };
diff --git a/platforms/cuda/src/CudaKernels.cpp b/platforms/cuda/src/CudaKernels.cpp
index 89f91be..59101bb 100644
--- a/platforms/cuda/src/CudaKernels.cpp
+++ b/platforms/cuda/src/CudaKernels.cpp
@@ -629,6 +629,24 @@ void CudaCalcCustomBondForceKernel::initialize(const System& system, const Custo
     info = new ForceInfo(force);
     cu.addForce(info);
 
+    // Record the tabulated functions.
+
+    map<string, Lepton::CustomFunction*> functions;
+    vector<pair<string, string> > functionDefinitions;
+    vector<const TabulatedFunction*> functionList;
+    tabulatedFunctions.resize(force.getNumTabulatedFunctions());
+    for (int i = 0; i < force.getNumTabulatedFunctions(); i++) {
+        functionList.push_back(&force.getTabulatedFunction(i));
+        string name = force.getTabulatedFunctionName(i);
+        functions[name] = cu.getExpressionUtilities().getFunctionPlaceholder(force.getTabulatedFunction(i));
+        int width;
+        vector<float> f = cu.getExpressionUtilities().computeFunctionCoefficients(force.getTabulatedFunction(i), width);
+        tabulatedFunctions[i].initialize<float>(cu, f.size(), "TabulatedFunction");
+        tabulatedFunctions[i].upload(f);
+        string arrayName = cu.getBondedUtilities().addArgument(tabulatedFunctions[i].getDevicePointer(), width == 1 ? "float" : "float"+cu.intToString(width));
+        functionDefinitions.push_back(make_pair(name, arrayName));
+    }
+
     // Record information for the expressions.
 
     globalParamNames.resize(force.getNumGlobalParameters());
@@ -637,7 +655,7 @@ void CudaCalcCustomBondForceKernel::initialize(const System& system, const Custo
         globalParamNames[i] = force.getGlobalParameterName(i);
         globalParamValues[i] = (float) force.getGlobalParameterDefaultValue(i);
     }
-    Lepton::ParsedExpression energyExpression = Lepton::Parser::parse(force.getEnergyFunction()).optimize();
+    Lepton::ParsedExpression energyExpression = Lepton::Parser::parse(force.getEnergyFunction(), functions).optimize();
     Lepton::ParsedExpression forceExpression = energyExpression.differentiate("r").optimize();
     map<string, Lepton::ParsedExpression> expressions;
     expressions["energy += "] = energyExpression;
@@ -673,9 +691,7 @@ void CudaCalcCustomBondForceKernel::initialize(const System& system, const Custo
         string argName = cu.getBondedUtilities().addArgument(buffer.getMemory(), buffer.getType());
         compute<<buffer.getType()<<" bondParams"<<(i+1)<<" = "<<argName<<"[index];\n";
     }
-    vector<const TabulatedFunction*> functions;
-    vector<pair<string, string> > functionNames;
-    compute << cu.getExpressionUtilities().createExpressions(expressions, variables, functions, functionNames, "temp");
+    compute << cu.getExpressionUtilities().createExpressions(expressions, variables, functionList, functionDefinitions, "temp");
     map<string, string> replacements;
     replacements["APPLY_PERIODIC"] = (force.usesPeriodicBoundaryConditions() ? "1" : "0");
     replacements["COMPUTE_FORCE"] = compute.str();
diff --git a/platforms/opencl/include/OpenCLKernels.h b/platforms/opencl/include/OpenCLKernels.h
index 841aed8..1b0f0ac 100644
--- a/platforms/opencl/include/OpenCLKernels.h
+++ b/platforms/opencl/include/OpenCLKernels.h
@@ -319,6 +319,7 @@ private:
     const System& system;
     OpenCLParameterSet* params;
     OpenCLArray globals;
+    std::vector<OpenCLArray> tabulatedFunctions;
     std::vector<std::string> globalParamNames;
     std::vector<cl_float> globalParamValues;
 };
diff --git a/platforms/opencl/src/OpenCLKernels.cpp b/platforms/opencl/src/OpenCLKernels.cpp
index a90211f..5246a0a 100644
--- a/platforms/opencl/src/OpenCLKernels.cpp
+++ b/platforms/opencl/src/OpenCLKernels.cpp
@@ -651,6 +651,29 @@ void OpenCLCalcCustomBondForceKernel::initialize(const System& system, const Cus
     info = new ForceInfo(force);
     cl.addForce(info);
 
+    int forceIndex;
+    for (forceIndex = 0; forceIndex < system.getNumForces() && &system.getForce(forceIndex) != &force; ++forceIndex)
+        ;
+    string prefix = "custom"+cl.intToString(forceIndex)+"_";
+    // Record the tabulated functions.
+
+    map<string, Lepton::CustomFunction*> functions;
+    vector<pair<string, string> > functionDefinitions;
+    vector<const TabulatedFunction*> functionList;
+    stringstream tableArgs;
+    tabulatedFunctions.resize(force.getNumTabulatedFunctions());
+    for (int i = 0; i < force.getNumTabulatedFunctions(); i++) {
+        functionList.push_back(&force.getTabulatedFunction(i));
+        string name = force.getTabulatedFunctionName(i);
+        functions[name] = cl.getExpressionUtilities().getFunctionPlaceholder(force.getTabulatedFunction(i));
+        int width;
+        vector<float> f = cl.getExpressionUtilities().computeFunctionCoefficients(force.getTabulatedFunction(i), width);
+        tabulatedFunctions[i].initialize<float>(cl, f.size(), "TabulatedFunction");
+        tabulatedFunctions[i].upload(f);
+        string arrayName = cl.getBondedUtilities().addArgument(tabulatedFunctions[i].getDeviceBuffer(), width == 1 ? "float" : "float"+cl.intToString(width));
+        functionDefinitions.push_back(make_pair(name, arrayName));
+    }
+
     // Record information for the expressions.
 
     globalParamNames.resize(force.getNumGlobalParameters());
@@ -659,7 +682,7 @@ void OpenCLCalcCustomBondForceKernel::initialize(const System& system, const Cus
         globalParamNames[i] = force.getGlobalParameterName(i);
         globalParamValues[i] = (cl_float) force.getGlobalParameterDefaultValue(i);
     }
-    Lepton::ParsedExpression energyExpression = Lepton::Parser::parse(force.getEnergyFunction()).optimize();
+    Lepton::ParsedExpression energyExpression = Lepton::Parser::parse(force.getEnergyFunction(), functions).optimize();
     Lepton::ParsedExpression forceExpression = energyExpression.differentiate("r").optimize();
     map<string, Lepton::ParsedExpression> expressions;
     expressions["energy += "] = energyExpression;
@@ -695,9 +718,9 @@ void OpenCLCalcCustomBondForceKernel::initialize(const System& system, const Cus
         string argName = cl.getBondedUtilities().addArgument(buffer.getMemory(), buffer.getType());
         compute<<buffer.getType()<<" bondParams"<<(i+1)<<" = "<<argName<<"[index];\n";
     }
-    vector<const TabulatedFunction*> functions;
-    vector<pair<string, string> > functionNames;
-    compute << cl.getExpressionUtilities().createExpressions(expressions, variables, functions, functionNames, "temp");
+    //vector<const TabulatedFunction*> functions;
+    //vector<pair<string, string> > functionNames;
+    compute << cl.getExpressionUtilities().createExpressions(expressions, variables, functionList, functionDefinitions, "temp");
     map<string, string> replacements;
     replacements["APPLY_PERIODIC"] = (force.usesPeriodicBoundaryConditions() ? "1" : "0");
     replacements["COMPUTE_FORCE"] = compute.str();
diff --git a/platforms/reference/src/ReferenceKernels.cpp b/platforms/reference/src/ReferenceKernels.cpp
index 739809f..d26b0d0 100644
--- a/platforms/reference/src/ReferenceKernels.cpp
+++ b/platforms/reference/src/ReferenceKernels.cpp
@@ -407,9 +407,15 @@ void ReferenceCalcCustomBondForceKernel::initialize(const System& system, const
             bondParamArray[i][j] = params[j];
     }
 
-    // Parse the expression used to calculate the force.
+    // Create custom functions for the tabulated functions.
 
-    Lepton::ParsedExpression expression = Lepton::Parser::parse(force.getEnergyFunction()).optimize();
+    map<string, Lepton::CustomFunction*> functions;
+    for (int i = 0; i < force.getNumTabulatedFunctions(); i++)
+        functions[force.getTabulatedFunctionName(i)] = createReferenceTabulatedFunction(force.getTabulatedFunction(i));
+
+    // Parse the various expressions used to calculate the force.
+
+    Lepton::ParsedExpression expression = Lepton::Parser::parse(force.getEnergyFunction(), functions).optimize();
     energyExpression = expression.createCompiledExpression();
     forceExpression = expression.differentiate("r").createCompiledExpression();
     for (int i = 0; i < numParameters; i++)
@@ -426,6 +432,12 @@ void ReferenceCalcCustomBondForceKernel::initialize(const System& system, const
     variables.insert(parameterNames.begin(), parameterNames.end());
     variables.insert(globalParameterNames.begin(), globalParameterNames.end());
     validateVariables(expression.getRootNode(), variables);
+
+    // Delete the custom functions.
+
+    for (auto& function : functions)
+        delete function.second;
+
     ixn = new ReferenceCustomBondIxn(energyExpression, forceExpression, parameterNames, energyParamDerivExpressions);
 }
 
diff --git a/serialization/src/CustomBondForceProxy.cpp b/serialization/src/CustomBondForceProxy.cpp
index 4f17afb..d8d211f 100644
--- a/serialization/src/CustomBondForceProxy.cpp
+++ b/serialization/src/CustomBondForceProxy.cpp
@@ -59,6 +59,9 @@ void CustomBondForceProxy::serialize(const void* object, SerializationNode& node
     for (int i = 0; i < force.getNumEnergyParameterDerivatives(); i++) {
         energyDerivs.createChildNode("Parameter").setStringProperty("name", force.getEnergyParameterDerivativeName(i));
     }
+    SerializationNode& functions = node.createChildNode("Functions");
+    for (int i = 0; i < force.getNumTabulatedFunctions(); i++)
+        functions.createChildNode("Function", &force.getTabulatedFunction(i)).setStringProperty("name", force.getTabulatedFunctionName(i));
     SerializationNode& bonds = node.createChildNode("Bonds");
     for (int i = 0; i < force.getNumBonds(); i++) {
         int p1, p2;
@@ -95,6 +98,9 @@ void* CustomBondForceProxy::deserialize(const SerializationNode& node) const {
             for (auto& parameter : energyDerivs.getChildren())
                 force->addEnergyParameterDerivative(parameter.getStringProperty("name"));
         }
+        const SerializationNode& functions = node.getChildNode("Functions");
+        for (auto& function : functions.getChildren())
+            force->addTabulatedFunction(function.getStringProperty("name"), function.decodeObject<TabulatedFunction>());
         const SerializationNode& bonds = node.getChildNode("Bonds");
         vector<double> params(force->getNumPerBondParameters());
         for (auto& bond : bonds.getChildren()) {
diff --git a/serialization/tests/TestSerializeCustomBondForce.cpp b/serialization/tests/TestSerializeCustomBondForce.cpp
index 67c0d7b..250a4c2 100644
--- a/serialization/tests/TestSerializeCustomBondForce.cpp
+++ b/serialization/tests/TestSerializeCustomBondForce.cpp
@@ -54,6 +54,10 @@ void testSerialization() {
     force.addBond(4, 0, params);
     params[0] = 2.1;
     force.addBond(3, 7, params);
+    vector<double> values(10);
+    for (int i = 0; i < 10; i++)
+        values[i] = sin((double) i);
+    force.addTabulatedFunction("f", new Continuous1DFunction(values, 0.5, 1.5));
     force.setUsesPeriodicBoundaryConditions(true);
 
     // Serialize and then deserialize it.
@@ -78,6 +82,19 @@ void testSerialization() {
     ASSERT_EQUAL(force.getNumEnergyParameterDerivatives(), force2.getNumEnergyParameterDerivatives());
     for (int i = 0; i < force.getNumEnergyParameterDerivatives(); i++)
         ASSERT_EQUAL(force.getEnergyParameterDerivativeName(i), force2.getEnergyParameterDerivativeName(i));
+    ASSERT_EQUAL(force.getNumTabulatedFunctions(), force2.getNumTabulatedFunctions());
+    for (int i = 0; i < force.getNumTabulatedFunctions(); i++) {
+        double min1, min2, max1, max2;
+        vector<double> val1, val2;
+        dynamic_cast<Continuous1DFunction&>(force.getTabulatedFunction(i)).getFunctionParameters(val1, min1, max1);
+        dynamic_cast<Continuous1DFunction&>(force2.getTabulatedFunction(i)).getFunctionParameters(val2, min2, max2);
+        ASSERT_EQUAL(force.getTabulatedFunctionName(i), force2.getTabulatedFunctionName(i));
+        ASSERT_EQUAL(min1, min2);
+        ASSERT_EQUAL(max1, max2);
+        ASSERT_EQUAL(val1.size(), val2.size());
+        for (int j = 0; j < (int) val1.size(); j++)
+            ASSERT_EQUAL(val1[j], val2[j]);
+    }
     ASSERT_EQUAL(force.usesPeriodicBoundaryConditions(), force2.usesPeriodicBoundaryConditions());
     ASSERT_EQUAL(force.getNumBonds(), force2.getNumBonds());
     for (int i = 0; i < force.getNumBonds(); i++) {
diff --git a/tests/TestCustomBondForce.h b/tests/TestCustomBondForce.h
index 16f83a3..9f23368 100644
--- a/tests/TestCustomBondForce.h
+++ b/tests/TestCustomBondForce.h
@@ -213,6 +213,51 @@ void testEnergyParameterDerivatives() {
     }
 }
 
+
+void testContinuous1DFunction() {
+    System system;
+    system.addParticle(1.0);
+    system.addParticle(1.0);
+    VerletIntegrator integrator(0.01);
+    CustomBondForce* forceField = new CustomBondForce("fn(r)+1");
+    vector<double> parameters(forceField->getNumPerBondParameters());
+    forceField->addBond(0, 1, parameters);
+    vector<double> table;
+    for (int i = 0; i < 21; i++)
+        table.push_back(sin(0.25*i));
+    cout << "tabke created \n";
+    forceField->addTabulatedFunction("fn", new Continuous1DFunction(table, 1.0, 6.0));
+    cout << "table added \n";
+    system.addForce(forceField);
+    cout << "FF added \n";
+    forceField->getTabulatedFunction(0);
+    cout << "table loaded \n";
+    Context context(system, integrator, platform);
+    cout << "context created \n";
+    vector<Vec3> positions(2);
+    positions[0] = Vec3(0, 0, 0);
+    for (int i = 1; i < 30; i++) {
+        double x = (7.0/30.0)*i;
+        positions[1] = Vec3(x, 0, 0);
+        context.setPositions(positions);
+        State state = context.getState(State::Forces | State::Energy);
+        const vector<Vec3>& forces = state.getForces();
+        double force = (x < 1.0 || x > 6.0 ? 0.0 : -cos(x-1.0));
+        double energy = (x < 1.0 || x > 6.0 ? 0.0 : sin(x-1.0))+1.0;
+        ASSERT_EQUAL_VEC(Vec3(-force, 0, 0), forces[0], 0.1);
+        ASSERT_EQUAL_VEC(Vec3(force, 0, 0), forces[1], 0.1);
+        ASSERT_EQUAL_TOL(energy, state.getPotentialEnergy(), 0.02);
+    }
+    for (int i = 1; i < 20; i++) {
+        double x = 0.25*i+1.0;
+        positions[1] = Vec3(x, 0, 0);
+        context.setPositions(positions);
+        State state = context.getState(State::Energy);
+        double energy = (x < 1.0 || x > 6.0 ? 0.0 : sin(x-1.0))+1.0;
+        ASSERT_EQUAL_TOL(energy, state.getPotentialEnergy(), 1e-4);
+    }
+}
+
 void runPlatformTests();
 
 int main(int argc, char* argv[]) {
@@ -223,6 +268,7 @@ int main(int argc, char* argv[]) {
         testIllegalVariable();
         testPeriodic();
         testEnergyParameterDerivatives();
+	testContinuous1DFunction();
         runPlatformTests();
     }
     catch(const exception& e) {
diff --git a/wrappers/python/src/swig_doxygen/swigInputConfig.py b/wrappers/python/src/swig_doxygen/swigInputConfig.py
index a31478a..f042da7 100755
--- a/wrappers/python/src/swig_doxygen/swigInputConfig.py
+++ b/wrappers/python/src/swig_doxygen/swigInputConfig.py
@@ -149,6 +149,7 @@ STEAL_OWNERSHIP = {("Platform", "registerPlatform") : [0],
                    ("CustomManyParticleForce", "addTabulatedFunction") : [1],
                    ("CustomCVForce", "addTabulatedFunction") : [1],
                    ("CustomCVForce", "addCollectiveVariable") : [1],
+                   ("CustomBondForce", "addTabulatedFunction") : [1],
                    ("CustomIntegrator", "addTabulatedFunction") : [1],
                    ("CompoundIntegrator", "addIntegrator") : [0],
 }
